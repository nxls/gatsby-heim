<div align="center">
  <h1>Heimlich</h1>  
  <sup>Self-managable website for Artists brought to you by <a href="//dailysh.it" target="_blank">NEXT LEVEL SHIT</a></sup>
</div>

## Writing

- [Write a new blog post]()
- [Edit/delete existing blog posts]()

## Work

- [Upload new work]()
- [Rename/remove existing work]()

## Work categories

- [Add new work category]()
- [Change/remove work categories]()

## heimlich@eirene.uberspace.de

### Server

- **eirene.uberspace.de**
  RSA
- **185.26.156.224**
  IPv6-Adresse
- **2a00:d0c0:200:0:3493:eeff:fe94:1775 (gemeinsam genutzt)**
  IPv4-Adresse
- **fzARt8Xb/1VxESV/5MwheQlz4Erjg1eN0xuN68vVKXI**
  Hostname

## Support

In any point of time you can reach us out: [+49 176 807 59 182](tel://+4917680759182) or [support@dailysh.it](mailto://support@dailysh.it?subject=[GITHUB] Larissa Heim needs support at her GitHub Repository)

## Deployment

### Clone

```bash
git clone git@github.com:nextlevelshit/nls-gatsby-heim.git
```

### Install Dependencies

```bash
npm i

# or

yarn
```
