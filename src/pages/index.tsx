import React from 'react';
import { Layout, Work, SEO } from '../components';
import { graphql } from 'gatsby';
import IWorkCategory from '../models/WorkCategory.model';

interface Props {
  data: {
    allWork: {
      workCategories: IWorkCategory[];
    };
  };
}

export default class IndexPage extends React.Component<Props> {
  public render() {
    const { data } = this.props;
    const { workCategories } = data.allWork;

    return (
      <Layout>
        <SEO />
        <Work data={workCategories} />
      </Layout>
    );
  }
}

export const IndexQuery = graphql`
  query {
    allWork: allDirectory(filter: { sourceInstanceName: { eq: "work" }, fields: { workCategory: { eq: true } } }) {
      workCategories: nodes {
        fields {
          title
        }
        workItems: children {
          id
          __typename
          ... on Directory {
            id
            fields {
              title
              description
            }
            fields {
              slug
            }
            workPictures: children {
              id
              __typename
              ... on File {
                id
                name
                childImageSharp {
                  id
                  fluid {
                    src
                    srcSet
                    aspectRatio
                    sizes
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`;
