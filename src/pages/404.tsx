import * as React from 'react';
import { Container, Layout, Title, SEO } from '../components';
import { Helmet } from 'react-helmet';
import config from '../../config/SiteConfig';

export const NotFoundPage = () => (
  <Layout>
    <Helmet title={`404 Not Found | ${config.siteTitle}`} />
    <SEO />
    <Container>
      <Title>NOT FOUND</Title>
      <p>You just hit a route that doesn&#39;t exist... the sadness.</p>
    </Container>
  </Layout>
);

export default NotFoundPage;

