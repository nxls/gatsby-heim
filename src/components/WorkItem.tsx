import React from 'react';
import styled from 'styled-components';
import Img from 'gatsby-image';
import config from '../../config/SiteConfig';
import typography from '../utils/typography';
import { media } from '../utils/media';
import { Link } from 'gatsby';
import theme from './../../config/Theme';
import { sidebarTop } from './Sidebar';
import IWorkItem from '../models/WorkItem.model';

const ClickArea: any = styled(Link)`
  display: flex;
  justify-content: space-around;
  align-items: flex-start;
  padding: ${theme.grid.gutter}px ${theme.grid.gridGutterHalf}px;
  margin-bottom: 30vh;
  transition: all 0ms;
  // background: ${config.backgroundColor};

  @media ${media.tablet} {
  }

  @media ${media.phone} {
  }

  &:hover,
  &:focus {
    h3 {
      // transform: translate3d(${typography.rhythm(0.3)}, 0, 0);
      color: ${theme.colors.black};
    }
  }

  img {
    margin-bottom: 0;
  }
`;

// const SmallPicture: any = styled.div`

// `;

const Picture: any = styled.div`
  display: block;
  min-height: 50vh;
  min-width: 50%;
  position: relative;
  padding-right: ${theme.grid.gridGutterHalf}rem;
`;

const WhiteSpace: any = styled.div`
  // flex: 1 0 auto;
  display: flex;
  width: 100%;
  height: 100%;
`;

const Title: any = styled.h3`
  position: sticky;
  top: ${sidebarTop};
  margin: 0 0 ${typography.rhythm(0.2)} ${typography.rhythm(0.8)};
  color: ${theme.colors.grey.default};
  font-weight: 200;
  font-size: ${typography.rhythm(0.5)};
  line-height: 1;
  transition: all 350ms;
`;

interface Props {
  data: IWorkItem;
}

export const WorkItem = ({ data }: Props) => {
  const { workPictures, fields } = data;

  if (workPictures.length > 0) {
    const image = workPictures[0].childImageSharp;
    // const Children = children
    //   .map(({ childImageSharp }, i) => (
    //     <SmallPicture key={i}>
    //       <Img fluid={childImageSharp.fluid} />
    //     </SmallPicture>
    //   ))
    //   .slice(0, 3);
    if (image) {
      return (
        <ClickArea to={fields.slug}>
          <Picture>
            <Img fluid={image.fluid} />
          </Picture>
          <WhiteSpace>
            <Title>{name}</Title>
            {/* {children.map(({ childImageSharp }, i) => (
                <SmallPicture key={i}>
                  <Img fluid={childImageSharp.fluid} />
                </SmallPicture>
              ))} */}
          </WhiteSpace>
        </ClickArea>
      );
    }
    return null;
  }
  return null;
}
