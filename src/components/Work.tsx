import React from 'react';
import config from '../../config/SiteConfig';
import styled from 'styled-components';
import { Container } from './Container';
import { Row } from './Row';
import { Column } from './Column';
import { WorkItem } from './WorkItem';
import typography from '../utils/typography';
import { Sidebar } from './Sidebar';
import slug from 'slug';
import theme from '../../config/Theme';
import IWorkCategory from '../models/WorkCategory.model';

interface WorkProps {
  data: IWorkCategory[];
}

export const WorkCategoryTitle: any = styled.h2`
  padding: ${typography.rhythm(0.6)} 0 ${typography.rhythm(4)} 0;
  font-size: ${typography.rhythm(0.8)};
  color: ${theme.colors.grey.dark};
`;

export const WorkCategoryContainer: any = styled.div``;

export const WorkItemRow: any = styled.div``;

const BleedingColumn: any = styled(Column)`
  margin-left: -${(100 / config.gridColumns * config.defaultColumnsLeft)}%;
`;

export class Work extends React.PureComponent<WorkProps> {
  render() {
    const { data } = this.props;
    const categories = data.map((node) => ({
      ...node,
      anchor: slug(node.fields.title, { lower: true }),
    }));

    return (
      <Container>
        <Row>
          <Column width={{ default: 2, phone: 0, tablet: 0 }}>
            <Sidebar>
              {config.siteDescription}
              {/* <NavigationBrand /> */}
              {/* <ul>
                {categories.map(({ name, anchor }, index) => (
                  <li key={index}>
                    <Link to={`/#${anchor}`}>{name}</Link>
                  </li>
                ))}
              </ul> */}
            </Sidebar>
          </Column>
          <BleedingColumn width={{ default: 12, phone: 12, tablet: 12 }}>
            {/* <Title>Work</Title> */}
            {categories.map(({ anchor, fields, workItems }, index) => (
              <WorkCategoryContainer key={index}>
                <a id={anchor} title={fields.title} />
                <WorkCategoryTitle id={anchor}>{fields.title}</WorkCategoryTitle>
                <WorkItemRow>
                  {workItems && workItems.reverse().map((item, i) => (
                    <WorkItem data={item} key={i} />
                  ))}
                </WorkItemRow>
              </WorkCategoryContainer>
            ))}
          </BleedingColumn>
        </Row>
      </Container>
    );
  }
}
