import React from 'react';
import styled from 'styled-components';
import theme from '../../config/Theme';
import { Container } from './Container';
import { Row } from './Row';
import { Column } from './Column';
import { Navigation } from './Navigation';
import { NavigationBrand } from './NavigationBrand';
import typography from '../utils/typography';


const HeaderWrapper: any = styled.header`
  position: sticky;
  display: flex;
  align-items: center;
  background-color: ${theme.colors.grey.ultraLight};
  padding: ${typography.rhythm(1)} 0;
  min-height: 10vh;
  width: 100%;
  line-height: ${typography.rhythm(theme.layout.headerHeightRatio)};
`;

export const Header = () => {
  return (
    <HeaderWrapper>
      <Container>
        <Row>
          <Column width={{ default: theme.grid.defaultColumnsLeft }}>
            <NavigationBrand />
          </Column>
          <Column width={{ default: theme.grid.defaultColumnsRight }}>
            <Navigation />
          </Column>
        </Row>
      </Container>
    </HeaderWrapper>
  );
}
