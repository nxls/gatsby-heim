import React from 'react';
import styled from 'styled-components';
import theme from '../../config/Theme';
import { Container } from './Container';
import { Row } from './Row';
import { Column } from './Column';
import typography from './../utils/typography';
import config from './../../config/SiteConfig';
import { Link } from 'gatsby';


const FooterWrapper = styled.div`
  background-color: ${theme.colors.grey.ultraLight};
  position: absolute;
  bottom: 0;
  width: 100%;
  line-height: ${typography.rhythm(theme.layout.footerHeightRatio)};
`;

const ColumnFirst = styled(Column)`
  display: flex;
  align-items: center;
`;

const ColumnCenter = styled(Column)`
  display: flex;
  justify-content: center;
  align-items: center;
`;

const ColumnLast = styled(Column)`
  display: flex;
  justify-content: flex-end;

`;

const Icon: any = styled.img`
  width: auto;
  height: 100%;
  max-height: ${typography.rhythm(1)};
  margin: 0 0 0 ${typography.rhythm(.2)};
`;


const AntiSocialMediaLink: any = styled(Link)`
  display: flex;
  align-items: center;
  color: ${typography.options.bodyColor};
  font-weight: ${typography.options.boldWeight};
  opacity: .6;
  transition: opacity 360ms;

  &:hover {
    opacity: 1;
  }
`;

export const Footer = () => {

  const InstagramLink = () => (
    <AntiSocialMediaLink to={config.instagramUrl} title='Auf Larissa Heims Instagram Profil gehen'>
      {config.instagramUser}
      <Icon src={config.instagramIcon} />
    </AntiSocialMediaLink>

  );

  const Maintainer = () => (
    <AntiSocialMediaLink to={config.maintainerURL} title={config.maintainerTitle}>
      <Icon src={config.maintainerLogo} />
    </AntiSocialMediaLink>
  );


  return (
    <FooterWrapper>
      <Container>
        <Row>
          <ColumnFirst width={{ default: 4, phone: 1 }}>
            <Maintainer/>
          </ColumnFirst>
          <ColumnCenter width={{ default: 4, phone: 4 }}>
            &copy; {config.siteBrand}. {new Date().getFullYear()}
          </ColumnCenter>
          <ColumnLast width={{ default: 4, phone: 1 }}>
            <InstagramLink/>
          </ColumnLast>
        </Row>
      </Container>
    </FooterWrapper>
  );

}
