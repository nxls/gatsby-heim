import styled from 'styled-components';
import typography from '../utils/typography';
import config from '../../config/SiteConfig';
import theme from '../../config/Theme';

export const sidebarTop = typography.rhythm(2);

export const Sidebar: any = styled.aside`
  position: sticky;
  top: ${sidebarTop};
  // font-family: ${config.headerFontFamily};
  letter-spacing: ${typography.rhythm(0.02)};
  font-size: ${theme.fontSize.small};
  font-weight: 300;
  // text-transform: uppercase;

  ul {
    display: none;
    list-style-type: none;
    margin: 0;
    padding: 0;
  }
`;
