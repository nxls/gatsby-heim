import styled from 'styled-components';
import { media } from '../utils/media';
import theme from '../../config/Theme'

interface Props {
  width: {
    default: number; // desktop
    tablet?: number;
    phone?: number;
  };
  withoutPadding?: boolean;
}

const percentPerColumn = 100 / theme.grid.columns;

const maxWidth = {
  desktop: ({ width }: Props) => percentPerColumn * width.default,
  tablet: ({ width }: Props) => percentPerColumn * (width.tablet ? width.tablet : width.default),
  phone: ({ width }: Props) => percentPerColumn * (width.phone ? width.phone : width.default),
};

export const Column = styled.div`
  ${(props: Props) => !props.withoutPadding && `
    padding-left: ${theme.grid.gutterHalf}rem;
    padding-right: ${theme.grid.gutterHalf}rem;
  `}

  max-width: ${maxWidth.desktop}%;
  flex: 0 0 ${maxWidth.desktop}%;
  ${({ width }: Props) => width.default && width.default === 0 && `display: none;`}

  @media ${media.tablet} {
    flex: 0 0 ${maxWidth.tablet}%;
    max-width: ${maxWidth.tablet}%;
    ${({ width }: Props) => width.tablet && width.tablet === 0 && `display: none;`}
  }
  @media ${media.phone} {
    flex: 0 0 ${maxWidth.phone}%;
    max-width: ${maxWidth.phone}%;
    ${({ width }: Props) => width.phone && width.phone === 0 && `display: none;`}
  }
`;


