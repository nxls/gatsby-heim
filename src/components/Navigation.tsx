import React from 'react';
import styled from 'styled-components';
import {graphql, Link, useStaticQuery} from 'gatsby';
import theme from '../../config/Theme';
import typography from '../utils/typography';
import {media} from '../utils/media';

const fontFamily = typography.options.headerFontFamily
    ? typography.options.headerFontFamily.join()
    : 'inherit';

const List = styled.ul`
  display: flex;
  height: 100%;
  align-items: center;
  list-style-type: none;
  margin: 0;
  padding: 0;
`;

const Item = styled.li`
  margin-left: ${typography.rhythm(1)};
  display: inline-block;
  margin: 0;
  padding: 0;
`;

const NavigationLink = styled(Link)`
  text-transform: uppercase;
  font-weight: 400;
  letter-spacing: 2px;
  font-family: ${fontFamily};
  margin-right: ${typography.rhythm(.8)};
  color: ${theme.colors.grey.default};
  // border-bottom: 2px solid ${theme.colors.grey.tint};

  @media ${media.phone} {
    margin-right: 0;
    margin-right: ${typography.rhythm(.4)};
    letter-spacing: 0;
  }

  &:hover,
  &:focus {
    color: ${theme.colors.grey.dark};
    // border-color: ${theme.colors.grey.dark};
  }

  &.active {
    color: ${theme.colors.primary};
    // border-color: ${theme.colors.secondary};
  }
`;

export const Navigation = () => {
  const {mainNavigation, allWork} = useStaticQuery<QueryProps>(graphql`
    {
      mainNavigation: allMarkdownRemark(filter: {frontmatter: {mainNavigation: {eq: true}}}, sort: {fields: frontmatter___sort}) {
        items: nodes {
          fields {
            slug
          }
          frontmatter {
            title
            description
          }
        }
      }
      allWork: allDirectory(filter: {sourceInstanceName: {eq: "work"}, fields: {workCategory: {eq: true}}}) {
        workCategories: nodes {
          fields {
            title
            slug
          }
        }
      }
    }
  `);

  const navigation = mainNavigation.items.map(({fields, frontmatter}) => ({
    slug: `/${fields.slug}/`,
    title: frontmatter.title,
  }));

  const categories = allWork.workCategories.map(({fields}) => fields);

  const ListItem = ({slug, title, description}: NavigationItemProps) => (
    <Item>
      <NavigationLink to={slug} title={description} activeClassName='active'>
        {title}
      </NavigationLink>
    </Item>
  );

  return (
    <List>
      {categories && categories.map((item, i) => <ListItem {...item} key={i}/>)}
      {navigation && navigation.map((item, i) => <ListItem {...item} key={i}/>)}
    </List>
  );
};

interface QueryProps {
  mainNavigation: {
    items: {
      fields: {
        slug: string;
      };
      frontmatter: {
        description: string;
        title: string;
      };
    }[];
  };
  allWork: {
    workCategories: {
      fields: {
        title: string;
        slug: string;
      };
    }[];
  }
}

interface NavigationItemProps {
  description?: string;
  slug: string;
  title: string;
}
