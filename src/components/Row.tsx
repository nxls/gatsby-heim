import styled from 'styled-components';
import config from '../../config/SiteConfig';

interface Props {
  withSpaceBetween?: boolean;
  withoutNegativeMargin?: boolean;
}

export const Row = styled.div`
  display: flex;
  flex-wrap: no-wrap;
  justify-content: ${(props: Props) => props.withSpaceBetween ? `space-between` : `flex-start`};
  ${(props: Props) => !props.withoutNegativeMargin && `
    margin-left: -${config.gridGutterHalf}rem;
    margin-right: -${config.gridGutterHalf}rem;
  `}
`;
