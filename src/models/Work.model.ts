import IWorkItem from './WorkItem.model';

interface IWork {
  title: string;
  items?: [IWorkItem];
}

export default IWork;
