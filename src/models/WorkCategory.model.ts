import IWorkItem from "./WorkItem.model";

interface IWorkCategory {
  anchor?: string;
  fields: {
    title: string;
    description?: string;
  };
  workItems?: IWorkItem[];
}

export default IWorkCategory;