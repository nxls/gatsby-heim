import IWorkPicture from './WorkPicture.model';

interface IWorkItem {
  fields: {
    title: string;
    description?: string;
    slug?: string;
  }
  workPictures: IWorkPicture[];
  size?: string;
}

export default IWorkItem;
