import { FluidObject } from 'gatsby-image';

interface IWorkPicture {
  childImageSharp: {
    fluid: FluidObject;
  };
}

export default IWorkPicture;
