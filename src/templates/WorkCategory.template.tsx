import React from 'react';
import Helmet from 'react-helmet';
import { graphql } from 'gatsby';
import { Layout, Container, Row, Column, SEO, WorkCategoryContainer, WorkCategoryTitle, WorkItemRow } from '../components';
import config from '../../config/SiteConfig';
import IWorkCategory from '../models/WorkCategory.model';
import { WorkItem } from '../components/WorkItem';


interface Props {
  data: {
    workCategory: IWorkCategory;
  };
  location: Location;
  pathContext: {
    id: string;
    title: string;
  };
}

const WorkCategoryPage= ({ data, pathContext}: Props) => {
  const { title } = pathContext;
  const { workItems } = data.workCategory;

  return (
    <Layout>
      <Helmet title={`${title} | ${config.siteTitle}`} />
      <SEO />
      <Container>
        <Row>
          <Column width={{ default: 12 }}>
            <WorkCategoryContainer>
              <WorkCategoryTitle>{title}</WorkCategoryTitle>
              <WorkItemRow>
                {workItems && workItems.reverse().map((item, i) => (
                  <WorkItem data={item} key={i} />
                ))}
              </WorkItemRow>
            </WorkCategoryContainer>
          </Column>
        </Row>
      </Container>
    </Layout>
  );
}

export const pageQuery = graphql`
  query($id: String!) {
    workCategory: directory(id: {eq: $id}) {
      fields {
        title
      }
      workItems: children {
        id
        ... on Directory {
          id
          fields {
            title
            description
          }
          fields {
            slug
          }
          workPictures: children {
            id
            ... on File {
              id
              name
              childImageSharp {
                id
                fluid {
                  src
                  srcSet
                  aspectRatio
                  sizes
                }
              }
            }
          }
        }
      }
    }
  }  
`;

export default WorkCategoryPage;
