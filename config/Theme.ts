const colors: IColors = {
  primary: '#9C1159',
  secondary: 'rgba(156, 17, 89, .34)',
  bg: '#fff',
  white: '#fff',
  black: '#000',
  grey: {
    dark: '#212529',
    default: '#6c757d',
    tint: '#C4C7CB',
    transparent: 'rgba(0, 0, 0, 0.2)',
    light: '#edeff1',
    ultraLight: '#f8f9fa',
  },
};

const transitions: ITransitions = {
  normal: '320s',
  quick: '180ms',
};

const fontSize: IFontSize = {
  small: '.75rem',
  big: '2.5rem',
};

const layout: ILayout = {
  headerHeightRatio: 2,
  footerHeightRatio: 2,
};

const grid: IGrid = {
  gutter: 1,
  gutterHalf: .5,
  columns: 12,

  defaultColumnsLeft: 6,
  defaultColumnsRight: 6,

  navigationColumnsLeft: 6,
  navigationColumnsRight: 6,
};

interface IColors {
  /**
   * Primary color for links, buttons and other styling elements
   */
  primary: string;
  /**
   * Secondary color hover states or other styling elements
   */
  secondary: 'rgba(156, 17, 89, .34)',
  /**
   * Background color
   */
  bg: '#fff',
  white: '#fff',
  black: '#000',
  grey: {
    dark: '#212529',
    default: '#6c757d',
    tint: '#C4C7CB',
    transparent: 'rgba(0, 0, 0, 0.2)',
    light: '#edeff1',
    ultraLight: '#f8f9fa',
  },
}

interface ITransitions {
  /**
   * Default transition properties
   */
  normal: string;
  /**
   * Transition properties for short animations and transitions
   */
  quick: string;
}

interface IFontSize {
  /**
   * Small font size for less important information
   */
  small: '.75rem',
  /**
   * Big font size for emphasizing information like titles
   */
  big: '2.5rem',
}

interface ILayout {
  /**
   * Aspect ratio of header height to viewport height
   */
  headerHeightRatio: 2,
  /**
   * Aspect ratio of footer height to viewport height
   */
  footerHeightRatio: 2,
}


interface IGrid {
  /**
   * Space between grid columns in rem
   */
  gutter: number;
  /**
   * Half measure of grid gutter
   */
  gutterHalf: number;
  /**
   * Total amount of columns
   */
  columns: number;
  /**
   * Default amount of columns for left side purpose
   */
  defaultColumnsLeft: number;
  /**
   * Default amount of columns for right side purpose
   */
  defaultColumnsRight: number;
  /**
   * Amount of navigations columns for left side purpose
   */
  navigationColumnsLeft: number;
  /**
   * Amount of navigations columns for right side purpose
   */
  navigationColumnsRight: number;
}

export default {
  colors,
  transitions,
  fontSize,
  grid,
  layout,
};
