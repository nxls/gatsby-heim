import theme from './Theme';

export default {
  /**
   * Prefix for all links. If you deploy your site to example.com/portfolio your pathPrefix should be "portfolio"
   */
  pathPrefix: 'heim-gatsby',
  /**
   * Navigation brand and site title
   */
  siteTitle: 'Larissa Heim',
  /**
   * Alternative Site title for SEO
   */
  siteTitleAlt: 'Larissa Heim - Kunststudierende in Stuttgart und Freiburg',
  /**
   * Domain of your site without trailing slash
   */
  siteUrl: 'https://larissaheim.de',
  /**
   * Language Tag on <html> element
   */
  siteLanguage: 'de',
  /**
   * Your image for og:image tag. You can find it in the /static folder
   */
  siteBanner: '/assets/banner.jpg',
  /**
   * Your image for favicons. You can find it in the /src folder
   */
  favicon: 'src/favicon.png',
  /**
   * Site brand name used for titles, navigation etc.
   */
  siteBrand: 'Larissa Heim',
  /**
   * Your site description
   */
  siteDescription: `Larissa Heim - Kunststudierende aus Freiburg, Artist from Freiburg in Germany`,
  /**
   * Author for schemaORGJSONLD
   */
  author: 'Larissa Heim',
  /**
   * Image for schemaORGJSONLD
   */
  siteLogo: '/assets/logo.png',
  /**
   * Name of maintainer
   */
  maintainerName: 'NEXT LEVEL SHIT',
  /**
   * Displayed title of maintainer
   */
  maintainerTitle: 'Konzipiert und realisiert von NEXT LEVEL SHIT',
  /**
   * Image of Maintainer
   */
  maintainerLogo: '/assets/icons/nextlevelshit-logo-black2.png',
  /**
   * URL of Maintainer
   */
  maintainerURL: 'https://dailysh.it',
  /**
   * Only these file extensions are supported for work pictures
   */
  allowedWorkExtensions: ['jpg', 'jpeg', 'png'],
  /**
   * Instagram user account including "@"
   */
  instagramUser: '@larissa_heim',
  /**
   * Full link to Instagram profile
   */
  instagramUrl: 'https://instagram.com/larissa_heim',
  /**
   * Picture or icon of the Instagram logo
   */
  instagramIcon: '/assets/icons/instagram.jpg',
  /**
   * Theme color for Web Manifest
   *
   * @see https://developers.google.com/web/fundamentals/web-app-manifest/
   */
  themeColor: theme.colors.primary,
  /**
   * Background color for Web Manifest
   *
   * @see https://developers.google.com/web/fundamentals/web-app-manifest/
   */
  backgroundColor: theme.colors.bg,

  /**
   * Header font family for typography.js
   *
   * @see https://kyleamathews.github.io/typography.js/
   */
  headerFontFamily: 'Poppins',
  /**
   * Body font family for typography.js
   *
   * @see https://kyleamathews.github.io/typography.js/
   */
  bodyFontFamily: 'Barlow',
  /**
   * Base font size for typography.js
   *
   * @see https://kyleamathews.github.io/typography.js/
   */
  baseFontSize: '18rem',
};
