const path = require(`path`)
const slash = require(`slash`)
const slug = require(`slug`)
const _ = require(`lodash`)
const config = require(`./config/SiteConfig`).default

function slugify(raw) {
  return slug(
    raw,
    {
      lower: true
    }
  )
}

exports.onCreateWebpackConfig = ({ stage, actions }) => {
  actions.setWebpackConfig({
    resolve: {
      modules: [path.resolve(__dirname, `src`), `node_modules`],
    },
  })
}

exports.onCreateNode = ({ node, getNodesByType, actions }) => {

  const {
    createNodeField,
    createParentChildLink
  } = actions

  if (node.sourceInstanceName === `work`) {
    // Prepare parent-child-relationship
    const parentDirectory = path.normalize(`${node.dir}/`)
    const parent = getNodesByType(`Directory`).find(
      n => path.normalize(`${n.absolutePath}/`) === parentDirectory
    )
    // Connect work workCategories and categories
    if (node.internal.type === `Directory`) {
      // Append parent-child-relationship
      if (parent) {
        node.parent = parent.id
        createParentChildLink({ child: node, parent: parent })
      }
      // Flag work categories for easier searchability 
      if (!node.relativeDirectory) {
        const title = node.name.replace(/\d+_/gm, ``);
        
        createNodeField({ node, name: `workCategory`, value: true })
        createNodeField({ node, name: `slug`, value: `/${slugify(title)}/` })
        createNodeField({ node, name: `title`, value: title })
      }
      // Flag work workCategories for better searchability
      if (node.relativeDirectory !== `..` && node.relativeDirectory !== ``) {
        const nameSplitted = node.name.split(" - ")
        const title = nameSplitted[0]
        const description = nameSplitted.length > 1 ? nameSplitted[1] : null

        createNodeField({ node, name: `slug`, value: `/work/${slugify(title)}/` })
        createNodeField({ node, name: `workItem`, value: true })
        createNodeField({ node, name: `title`, value: title})
        createNodeField({ node, name: `description`, value: description})
      }     
    }
    // Connect work pictures
    if (node.internal.type === `File`) {
      if (config.allowedWorkExtensions.find(ext => ext === node.extension)) {
        // Flag work picture for better searchability
        createNodeField({node, name: `workPicture`, value: true })
        // Append parent-child-relationship
        if (parent) {
          createParentChildLink({ child: node, parent: parent })
        }
      }
    }
  }

  if (node.internal.type === `MarkdownRemark` && _.has(node, `frontmatter`) && _.has(node.frontmatter, `title`)) {
    createNodeField({ node, name: `slug`, value: slugify(node.frontmatter.title) });
  }
}

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions

  await Promise.all([
    createWorkItemPages({ createPage, graphql }),
    createMarkdownPages({ createPage, graphql }),
    createWorkCategoryPages({ createPage, graphql })
  ])
}

const createWorkItemPages = ({ createPage, graphql }) => {

  return graphql(
    `
    {
      allDirectory(
        filter: {
          sourceInstanceName: { eq: "work" }
          fields: { workItem: { eq: true }}
        }
      ) {
        edges {
          node {
            id
            name
            fields {
              title
            }
          }
        }
      }
    }
    `
  ).then(result => {
    if (result.errors) {
      throw result.errors
    }
    const itemTemplate = path.resolve(`src/templates/WorkItem.template.tsx`)
    const items = result.data.allDirectory.edges

    items.forEach(({ node }, index) => {
      const prev = index === 0 ? items[items.length - 1].node : items[index - 1].node;
      const next = index === items.length - 1 ? items[0].node : items[index + 1].node;

      createPage({
        path: `/work/${slugify(node.fields.title)}/`,
        component: slash(itemTemplate),
        context: {
          id: node.id,
          next,
          prev
        },
      })
    })
  })
}

const createMarkdownPages = async ({ createPage, graphql }) => {

  return graphql(
    `
    {
      allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/pages/" } }
        limit: 10000
      ) {
        edges {
          node {
            id
            fields {
              slug
            }
            headings {
              value
            }
          }
        }
      }
    }
    `
  ).then(result => {
    if (result.errors) {
      throw result.errors
    }
    const pages = result.data.allMarkdownRemark.edges
    const pageTemplate = path.resolve(`src/templates/Page.template.tsx`)

    pages.forEach(({ node }, index) => {
      createPage({
        path: `/${node.fields.slug}/`,
        component: slash(pageTemplate),
        context: {
          id: node.id,
          headings: node.headings.map(({ value }) => ({
            value,
            slug: slugify(value)
          }))
        },
      })
    })
  })
}

const createWorkCategoryPages = ({ createPage, graphql }) => {

  return graphql(
    `
    {
      allWork: allDirectory(
        filter: {
          sourceInstanceName: { eq: "work" }
          fields: { workCategory: { eq: true }}
        }
      ) {
        workCategories: nodes {
          id
          fields {
            title
            slug
          }
        }
      }
    }
    `
  ).then(result => {
    if (result.errors) {
      throw result.errors
    }
    const categoryTemplate = path.resolve(`src/templates/WorkCategory.template.tsx`)
    const { workCategories } = result.data.allWork

    workCategories.forEach(({ id, fields }, index) => {
      const { title, slug } = fields;

      createPage({
        path: slug,
        component: slash(categoryTemplate),
        context: {
          id,
          title
        },
      })
    })
  })
}
